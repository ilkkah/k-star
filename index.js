/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/main/docs/suggestions.md
 */
let KStar;
module.exports = (KStar = (function() {
  KStar = class KStar {
    static initClass() {
  
      this.prototype.view =  __dirname;
      this.prototype.name = 'k-star';
    }

    init() {}

    create() {
  	  const name = this.model.get('item');
  	  return this.model.set('selected', !!window.localStorage.getItem(name));
}

    destroy() {}

    select() {
  	  const now = !!this.model.get('selected');
  	  const name = this.model.get('item');
  	  if (now) {
  		  window.localStorage.removeItem(name);
  		  this.model.set('selecting', 'unselecting');
  	  } else {
  		  window.localStorage.setItem(name, true);
  		  this.model.set('selecting', 'selecting');
}

  	  return this.model.set('selected', !now);
}
  };
  KStar.initClass();
  return KStar;
})());